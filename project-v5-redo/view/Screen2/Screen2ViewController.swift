//
//  Screen2ViewController.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit

class Screen2ViewController: UIViewController {
    var project: Project!
    @IBOutlet weak var galleryView: GalleryView!
    @IBOutlet weak var editView: EditView!
    @IBOutlet weak var sliderView: SliderView!
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryView.clipsToBounds = true
        editView.clipsToBounds = true
        galleryView.delegate = self
        sliderView.delegate = self
        sliderView.isHidden = true
        galleryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideGallery)))
        project.checkCachedProject { photo in
            self.galleryView.project = self.project
            self.galleryView.presentPhoto(photo)
        }
    }
    
    @objc func didTapOutsideGallery() {
        galleryView.deleteButton.isHidden = true
        editView.setLayersHidden(true)
        sliderView.isHidden = true
    }
    
    @IBAction func onBack(_ sender: Any) {
        showAlert(title: "Save Project", message: "Save change to this project?", leftActionText: "Don't save", rightActionText: "Save") { _ in
            self.navigationController?.popViewController(animated: true)
        } rightActionHandler: { _ in
            self.project.saveProject()
            self.navigationController?.popViewController(animated: true)
        }

    }
    @IBAction func exportPhoto(_ sender: Any) {
        galleryView.exportPhoto()
    }
}

extension Screen2ViewController: GalleryViewDelegate {
    func imageRemoved() {
        editView.setLayersHidden(true)
    }
    
    func imageSelected(_ image: CustomImageView) {
        editView.drawForView(image)
        sliderView.isHidden = false
        sliderView.value = image.alpha
        sliderView.setNeedsDisplay()
    }
}

extension Screen2ViewController: SliderViewDelegate {
    func alphaChanged(_ alpha: Double) {
        galleryView.updateOpacity(alpha)
    }
    
    
}
