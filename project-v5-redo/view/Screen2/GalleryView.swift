//
//  GalleryView.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit

protocol GalleryViewDelegate: AnyObject {
    func imageSelected(_ image: CustomImageView)
    func imageRemoved()
}

class GalleryView: UIView {
    var project: Project!
    var listId: [String]  = []
    var selected: Int = -1
    let deleteButton = CustomButton()
    var currentImageView: CustomImageView?
    var didLoad = false
    weak var delegate: GalleryViewDelegate?
    
    override func draw(_ rect: CGRect) {
        if didLoad { return }
        setUpDeleteButton()
        didLoad = true
    }
    
    private func setUpDeleteButton() {
        let buttonSize: Int = 20
        deleteButton.frame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        deleteButton.layer.cornerRadius = CGFloat(buttonSize/2)
        deleteButton.backgroundColor = .red
        let centerWhiteView = UIView()
        centerWhiteView.frame = CGRect(x: 0, y: 0, width: 8, height: 2)
        centerWhiteView.backgroundColor = .white
        centerWhiteView.center = deleteButton.convert(deleteButton.center, from:deleteButton)
        deleteButton.addSubview(centerWhiteView)
        addSubview(deleteButton)
        deleteButton.isHidden = true
        deleteButton.addTarget(self, action: #selector(handleDeletePhoto), for: .touchUpInside)
    }
    
    @objc func handleDeletePhoto() {
        currentImageView?.removeFromSuperview()
        project.photos.remove(at: selected)
        listId.remove(at: selected)
        deleteButton.isHidden = true
        if let d = delegate {
            d.imageRemoved()
        }
    }
    
    func presentPhoto(_ photo: Photo) {
        let image = CustomImageView(image: photo.image)
        image.id = photo.id
        image.frame = photo.frame
        image.transform = photo.transform
        image.alpha = photo.alpha
        addSubview(image)
        setUpImageView(image)
        listId.append(photo.id)
    }
    
    private func setUpImageView(_ image: CustomImageView) {
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:))))
        image.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(_:))))
        image.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(didPinch(_:))))
        image.addGestureRecognizer(UIRotationGestureRecognizer(target: self, action: #selector(didRotate(_:))))
    }
    
    @objc func didTap(_ sender: UITapGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else { return }
        notifyChange(image)
        
    }
    
    @objc func didPan(_ sender: UIPanGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else { return }
        if sender.state == .began {
            image.beginT = image.transform
        }
        if sender.state == .changed {
            let translation = sender.translation(in: self)
            image.transform = image.beginT.translatedBy(x: translation.x, y: translation.y)
        }
        notifyChange(image)
    }
    
    @objc func didPinch(_ sender: UIPinchGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else { return }
        if sender.state == .began {
            image.beginT =  image.transform
        }
        if sender.state == .changed {
            image.transform = image.beginT.scaledBy(x: sender.scale, y: sender.scale)
        }
        notifyChange(image)
    }
    
    @objc func didRotate(_ sender: UIRotationGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else { return }
        if sender.state == .began {
            image.beginT = image.transform
        }
        if sender.state == .changed {
            image.transform = image.beginT.rotated(by: sender.rotation)
        }
        notifyChange(image)
    }
    
    private func notifyChange(_ image: CustomImageView) {
        guard let i = listId.firstIndex(of: image.id) else { return }
        self.bringSubviewToFront(image)
        if let d = delegate {
            d.imageSelected(image)
        }
        currentImageView = image
        updateDeleteButtonFrame(image)
        selected = i
        project.photos[selected].save(image)
        
    }
    
    private func updateDeleteButtonFrame(_ image: CustomImageView) {
        let imageTransform = image.transform
        let scale = sqrt(Double(imageTransform.a * imageTransform.a + imageTransform.c * imageTransform.c))
        let center = image.convert(CGPoint(x: image.bounds.midX, y: -15/scale), to: self)
        deleteButton.center = center
        deleteButton.isHidden = false
        bringSubviewToFront(deleteButton)
    }
    
    func updateOpacity(_ alpha: Double) {
        guard let image = currentImageView else { return }
        image.alpha = alpha
        project.photos[selected].save(image)
    }
    
    func exportPhoto() {
        
    }
}

class CustomImageView: UIImageView {
    var id: String = ""
    var beginT: CGAffineTransform = .identity
}

class CustomButton: UIButton {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return bounds.insetBy(dx: -10, dy: -10).contains(point)
    }
}
