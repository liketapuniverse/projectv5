//
//  EditView.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit

class EditView: UIView {
    let line = CAShapeLayer()
    let circle = CAShapeLayer()
    let radius: Double = 6
    
    func drawForView(_ image: CustomImageView) {
        setLayersHidden(false)
        let topLeft = image.convert(CGPoint.zero, to: self)
        let topRight = image.convert(CGPoint(x: image.bounds.maxX, y: 0), to: self)
        let bottomLeft = image.convert(CGPoint(x: 0, y: image.bounds.maxY), to: self)
        let bottomRight = image.convert(CGPoint(x: image.bounds.maxX, y: image.bounds.maxY), to: self)
        let linePath = UIBezierPath()
        linePath.move(to: topLeft)
        linePath.addLine(to: topRight)
        linePath.addLine(to: bottomRight)
        linePath.addLine(to: bottomLeft)
        linePath.close()
        line.path = linePath.cgPath
        line.fillColor = UIColor.clear.cgColor
        line.strokeColor = UIColor.blue.cgColor
        line.lineWidth = 3
        
        let circlePath = UIBezierPath()
        for p in [topLeft, topRight, bottomRight, bottomLeft] {
            circlePath.append(UIBezierPath(arcCenter: CGPoint(x: p.x, y: p.y), radius: radius, startAngle: 0, endAngle: .pi*2, clockwise: true))
        }
        circle.path = circlePath.cgPath
        circle.fillColor = UIColor.blue.cgColor
        
        layer.addSublayer(line)
        layer.addSublayer(circle)
    }
    
    func setLayersHidden(_ hidden: Bool) {
        line.isHidden = hidden
        circle.isHidden = hidden
    }
}
