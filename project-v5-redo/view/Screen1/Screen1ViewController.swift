//
//  ViewController.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit

class Screen1ViewController: UIViewController {
    
    //    let mockProjects = [Project(id: "1", name: "Hello"), Project(id: "2", name: "World")]
    var model = ListProject.shared
    @IBOutlet weak var projectTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(AppConstants.didLoadProjects), object: nil, queue: nil) { _ in
            DispatchQueue.main.async {
                self.projectTableView.reloadData()
            }
        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func addProject(_ sender: Any) {
        let alert = UIAlertController(title: "Add project", message: "Enter project name", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Your project name here"
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if (textField?.text != "") {
                let project = Project(id: UUID().uuidString, name: (textField?.text!)!)
                self.model.projects.append(project)
                self.projectTableView.beginUpdates()
                self.projectTableView.insertRows(at: [IndexPath(row: self.model.projects.count - 1 , section: 0)], with: .automatic)
                self.projectTableView.endUpdates()
                self.model.saveProjectsList()
            } else {
                let emptyTextAlert = UIAlertController(title: "Empty text", message: "Please don't leave name blank", preferredStyle: .alert)
                emptyTextAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(emptyTextAlert, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension Screen1ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = projectTableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell") as? ProjectTableViewCell else { return UITableViewCell()}
        cell.nameLabel.text =  model.projects[indexPath.row].name
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Screen2ViewController") as? Screen2ViewController else { return }
        vc.project = model.projects[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension Screen1ViewController: ProjectTableViewCellDelegate {
    func deleteProject(_ cell: ProjectTableViewCell) {
        if let indexPath = projectTableView.indexPath(for: cell) {
            model.projects.remove(at: indexPath.row)
            projectTableView.deleteRows(at: [indexPath], with: .fade)
            model.saveProjectsList()
        }
    }
}
