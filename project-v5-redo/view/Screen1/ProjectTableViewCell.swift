//
//  ProjectTableViewCell.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit
protocol ProjectTableViewCellDelegate: AnyObject {
    func deleteProject(_ cell: ProjectTableViewCell)
}

class ProjectTableViewCell: UITableViewCell {
    var beginX: CGFloat = 0
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var trailingConstant: NSLayoutConstraint!
    weak var delegate: ProjectTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func draw(_ rect: CGRect) {
        trailingConstant.constant = 0
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(_:))))
    }
    
    @objc func didPan(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began {
            beginX = trailingConstant.constant
        }
        if sender.state == .changed {
            let translation = sender.translation(in: self)
            trailingConstant.constant = beginX - translation.x
        }
        if sender.state == .ended || sender.state == .cancelled {
            // limit max min
            var current = trailingConstant.constant
            current = max(0, current)
            current = min(current, 50)
            current = current<20 ? 0 : 50
            trailingConstant.constant = current
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }
            
        }
    }
    @IBAction func deleteProject(_ sender: Any) {
        if let d = delegate {
            d.deleteProject(self)
        }
    }
}
