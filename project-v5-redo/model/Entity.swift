//
//  Entity.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit

class CodableProject: Codable {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

class Photo {
    let id: String
    let image: UIImage
    var frame: CGRect
    var transform: CGAffineTransform
    var alpha: Double
    init(id: String, image: UIImage, frame: CGRect, transform: CGAffineTransform, alpha: Double) {
        self.id = id
        self.image = image
        self.frame = frame
        self.transform = transform
        self.alpha = alpha
    }
    func save(_ image: CustomImageView) {
        let t = image.transform
        image.transform = .identity
        frame = image.frame
        transform = t
        image.transform = t
        print("alpha \(image.alpha)")
        alpha = image.alpha
    }
    
    static func load(fileName: String, photo: CodablePhoto) -> Photo? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {return nil}
        let fileURL = url.appendingPathComponent("\(fileName).jpg")
        guard let image = UIImage(contentsOfFile: fileURL.path) else { return nil}
        return Photo(id: photo.imageId, image: image, frame: photo.rect, transform: photo.transform, alpha: photo.alpha)
    }
}

class CodablePhoto: Codable {
    let imageId, projectId: String
    let rect: CGRect
    let transform: CGAffineTransform
    let alpha: Double
    init(imageId:  String, projectId: String, rect: CGRect,transform: CGAffineTransform, alpha: Double) {
        self.imageId = imageId
        self.projectId = projectId
        self.rect = rect
        self.transform = transform
        self.alpha = alpha
    }
}
