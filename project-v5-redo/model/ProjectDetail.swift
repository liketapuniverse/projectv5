//
//  ProjectDetail.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit
import Alamofire
import SVProgressHUD

class Project {
    let id: String
    let name: String
    var photos: [Photo] = []
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func checkCachedProject(completion: @escaping (Photo) -> Void) {
        photos = []
        if UserDefaults.standard.bool(forKey: "didCache-\(id)") {
            guard let listCodablePhoto = readListCodablePhoto() else {
                getDetailFromApi(completion: completion)
                return
            }
            for codablePhoto in listCodablePhoto {
                if codablePhoto.projectId == self.id {
                    guard let toAddPhoto = Photo.load(fileName: codablePhoto.imageId, photo: codablePhoto) else { continue }
                    completion(toAddPhoto)
                    photos.append(toAddPhoto)
                }
            }
        } else {
            getDetailFromApi(completion: completion)
        }
    }
    
    func getDetailFromApi(completion: @escaping (Photo) -> Void) {
        SVProgressHUD.show()
        AF.request("\(AppConstants.endPoint)/xprojectdetail", method: .post, parameters: ["id": Int(id)]).responseJSON { response in
            guard let json = response.value as? [String: Any],
                  let resPhotos = json["photos"] as? [[String: Any]]
            else {
                SVProgressHUD.dismiss()
                return
            }
            var count = 0
            for resPhoto in resPhotos {
                guard let url = resPhoto["url"] as? String,
                      let frame = resPhoto["frame"] as? [String: Any],
                      let x = frame["x"] as? Double,
                      let y = frame["y"] as? Double,
                      let width = frame["width"] as? Double,
                      let height = frame["height"] as? Double
                else { continue}
                AF.request(url).response { response in
                    count += 1
                    if count == resPhotos.count {
                        SVProgressHUD.dismiss()
                    }
                    guard let data = response.data else { return}
                    if let image = UIImage(data: data) {
                        let photo = Photo(id: UUID().uuidString, image: image, frame: CGRect(x: x, y: y, width: width, height: height), transform: .identity, alpha: 1)
                        self.photos.append(photo)
                        completion(photo)
                    }
                }
            }
        }
    }
    
    func saveProject() {
        var listCodablePhoto: [CodablePhoto] = []
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        for photo in photos {
            let codablePhoto = CodablePhoto(imageId: photo.id, projectId: id, rect: photo.frame, transform: photo.transform, alpha: photo.alpha)
            listCodablePhoto.append(codablePhoto)
            let fileUrl = url.appendingPathComponent("\(photo.id).jpg")
            if let data = photo.image.jpegData(compressionQuality: 1.0), !FileManager.default.fileExists(atPath: fileUrl.path) {
                do {
                    try data.write(to: fileUrl)
                    print("file saved at \(fileUrl.path)")
                    
                } catch {
                    print("error saving file \(error)")
                    continue
                }
            }
        }
        
        guard let data = try? JSONEncoder().encode(listCodablePhoto) else { return }
        let encodedString = String(data: data, encoding: .utf8)
        print("encoded list photo \(encodedString)")
        let fileUrl = url.appendingPathComponent("listPhoto\(self.id).json")
        do {
           try encodedString?.write(to: fileUrl, atomically: false, encoding: .utf8)
            UserDefaults.standard.set(true, forKey: "didCache-\(id)")
            ListProject.shared.saveProjectsList()
        } catch {
            print(error)
        }
        
    }
    
    func readListCodablePhoto() -> [CodablePhoto]? {
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let fileUrl = dir.appendingPathComponent("listPhoto\(self.id).json")
        guard let jsonString = try? String(contentsOf: fileUrl),
              let data = jsonString.data(using: .utf8),
              let listPhoto = try? JSONDecoder().decode([CodablePhoto].self, from: data)
        else { return nil}
        return listPhoto
    }
}


