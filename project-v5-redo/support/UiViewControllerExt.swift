//
//  UiViewExt.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, leftActionText: String? = nil, rightActionText: String, leftActionHandler: ((Any)-> Void)? = nil, rightActionHandler: @escaping (Any)->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if leftActionText != nil {
            let leftAction = UIAlertAction(title: leftActionText, style: .default, handler: leftActionHandler)
            alert.addAction(leftAction)
        }
        let rightAction = UIAlertAction(title: rightActionText, style: .default, handler: rightActionHandler)
        alert.addAction(rightAction)
        present(alert, animated: true, completion: nil)
    }
}
