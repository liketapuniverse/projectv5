//
//  AppConstants.swift
//  project-v5-redo
//
//  Created by admin on 27/10/2021.
//

import Foundation

class AppConstants {
    static let endPoint = "https://tapuniverse.com"
    static let didLoadProjects = "didLoadProjects"
    static let projectsListFilename = "projectsList.json"
}
